# Omp-Task-lab7
## Finite difference time domain method (FDTD)
Finite-difference time-domain or Yee's method (named after the Chinese American applied mathematician Kane S. Yee, born 1934) is a numerical analysis technique used for modeling computational electrodynamics (finding approximate solutions to the associated system of differential equations). Since it is a time-domain method, FDTD solutions can cover a wide frequency range with a single simulation run, and treat nonlinear material properties in a natural way.

The FDTD method belongs in the general class of grid-based differential numerical modeling methods (finite difference methods). The time-dependent Maxwell's equations (in partial differential form) are discretized using central-difference approximations to the space and time partial derivatives

![clusters](img/FDTD_Yee_grid_2d-3d.svg.png)


### Description
In this problem we have to parallelize a existing **FDTD** implementation using **omp task**


## Hints
- analyze the dependence between each iteration.
- you just have to change  **kernel_fdtd_2d** method inside **fdtd-2d-serial.c**   
- perform a diff between sequential and parallel output 

### Compiling and running
```shell
./build.sh
./fdtd > output
```
 ### notes
 - The matrix used in the calculation is generated arbitrarily.
 
 - The matrix dimensions can be found in **fdtd-2d.h**
 
 - your parallel version should be at least slightly faster than the serial one.
 
 ### credits
 
 - Louis-Noel Pouchet, [Polybench](http://web.cse.ohio-state.edu/~pouchet.2/software/polybench/)
 
 
 #### TODO:
 
 1. Fork the repository to your own account.
 2. Clone your forked repository on your computer.
 3. Study the code using your favorite editor.
 4. Parallelize the code using OpenMP.
 5. Compare performance between serial and parallel version. Determine the speedup using the test **t13**


Anything missing? Ideas for improvements? Make a pull request.
