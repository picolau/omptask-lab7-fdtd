#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

/* Include polybench common header. */

#include <omp.h>
/* Include benchmark-specific header. */
/* Default data type is double, default size is 50x1000x1000. */
#include "fdtd-2d.h"

#define NUM_THREADS 30

/* Array initialization. */
static
void init_array(int nx,
                int ny,
                double **ex,
                double **ey,
                double **hz,
                double *_fict_) {
    int i, j;
    for (i = 0; i < ny; i++)
        _fict_[i] = (double) i;
    for (i = 0; i < nx; i++)
        for (j = 0; j < ny; j++) {
            ex[i][j] = ((double) i * (j + 1)) / nx;
            ey[i][j] = ((double) i * (j + 2)) / ny;
            hz[i][j] = ((double) i * (j + 3)) / nx;
        }
}



//}

/* DCE code. Must scan the entire live-out data.
   Can be used also to check the correctness of the output. */
static
void print_array(int nx,
                 int ny,
                 double **ex,
                 double **ey,
                 double **hz) {
    int i, j;

    for (i = 0; i < nx; i++)
        for (j = 0; j < ny; j++) {
            printf(DATA_PRINTF_MODIFIER, ex[i][j]);
            printf(DATA_PRINTF_MODIFIER, ey[i][j]);
            printf(DATA_PRINTF_MODIFIER, hz[i][j]);
            if ((i * nx + j) % 20 == 0)
                printf("\n");
        }
    printf("\n");
}


/* Main computational kernel. The whole function will be timed,
   including the call and return. */
static
void kernel_fdtd_2d(int tmax,
                    int nx,
                    int ny,
                    double **ex,
                    double **ey,
                    double **hz,
                    double *_fict_) {
    int t, i, j;

#pragma scop
//you need to  parallelize  the  following  code:
    #pragma omp parallel default(none) private(t, i, j) shared(tmax, nx, ny, _fict_, ex, ey, hz)
    #pragma omp single nowait
    for (t = 0; t < tmax; t++) {
        #pragma omp task default(none) depend(out: ey)\
                shared(_fict_, nx, ny, hz, ey) private(i, j) firstprivate(t)
        {
            for (j = 0; j < ny; j++)
                ey[0][j] = _fict_[t];

            for (i = 1; i < nx; i++)
                for (j = 0; j < ny; j++)
                    ey[i][j] = ey[i][j] - 0.5 * (hz[i][j] - hz[i - 1][j]);
        }

        #pragma omp task default(none) depend(out: ex)\
                shared(ex, hz, nx, ny) private(i, j)
        for (i = 0; i < nx; i++)
            for (j = 1; j < ny; j++)
                ex[i][j] = ex[i][j] - 0.5 * (hz[i][j] - hz[i][j - 1]);

        #pragma omp task default(none) depend(in: ex, ey)\
                shared(ex, ey, hz, nx, ny) private(i, j)
        for (i = 0; i < nx - 1; i++)
            for (j = 0; j < ny - 1; j++)
                hz[i][j] = hz[i][j] - 0.7 * (ex[i][j + 1] - ex[i][j] + ey[i + 1][j] - ey[i][j]);

        #pragma omp taskwait
    };


// -----------------------------------------------------------------
#pragma endscop
}

double **allocateMatrix() {
    double **mat = malloc(sizeof(double *) * NX);

    for (int i = 0; i < NX; ++i) {
        mat[i] = malloc(sizeof(double) * NY);
    }

    return mat;
}

void freeMatrix(double **mat) {
    for (int i = 0; i < NX; ++i) {
        free(mat[i]);

    }

    free(mat);
}

int main(int argc, char **argv) {
    /* Retrieve problem size. */
    int tmax = TMAX;
    int nx = NX;
    int ny = NY;

    /* Variable declaration/allocation. */
    double **ex = allocateMatrix();
    double **ey = allocateMatrix();
    double **hz = allocateMatrix();
    double *_fict_ = malloc(sizeof(double) * NY);


    /* Initialize array(s). */
    init_array(nx, ny, ex, ey, hz, _fict_);

    double e, s;
    //use this to measure the obtained speedup
    s = omp_get_wtime();
//    /* Run kernel. */
    kernel_fdtd_2d(tmax, nx, ny, ex, ey, hz, _fict_);
    e = omp_get_wtime();
    printf("Time took: %lf\n", e - s);

    /*print resulting matrices */
    //print_array(nx, ny, ex, ey, hz);
    freeMatrix(ex);
    freeMatrix(ey);
    freeMatrix(hz);
    free(_fict_);

    return 0;
}
